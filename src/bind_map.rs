use once_cell::sync::Lazy;
use crate::{Bind, MouseButton, KeybdKey};
use std::hash::Hash;
use std::sync::Mutex;
use std::collections::HashMap;

pub type SafeBindMap<K> = Lazy<Mutex<HashMap<K, Bind>>>;

pub static KEYBD_BINDS: SafeBindMap<KeybdKey> = Lazy::new(|| Mutex::default());
pub static MOUSE_BINDS: SafeBindMap<MouseButton> = Lazy::new(|| Mutex::default());

pub trait BindMap<K: Eq + Hash> {
    fn insert_bind(&self, key: K, bind: Bind);
    fn remove_bind(&self, key: K) -> Option<Bind>;
}

impl<K: Eq + Hash> BindMap<K> for SafeBindMap<K> {
    fn insert_bind(&self, key: K, bind: Bind) {
        self.lock().unwrap().insert(key, bind);
    }

    fn remove_bind(&self, key: K) -> Option<Bind> {
        self.lock().unwrap().remove(&key)
    }
}
