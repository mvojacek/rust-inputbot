mod bind;
mod bind_map;
mod bindable_key;

pub mod keyboard;
pub mod mouse;

pub use bind::{Bind, BlockInput};
pub use bindable_key::{BindableKey, BindableKeyAll};
pub use keyboard::KeybdKey;
pub use mouse::MouseButton;

#[cfg(target_os = "linux")]
pub use crate::linux::*;
#[cfg(target_os = "windows")]
pub use crate::windows::*;

#[cfg(target_os = "linux")]
mod linux;
#[cfg(target_os = "windows")]
mod windows;
