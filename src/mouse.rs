use strum_macros::EnumIter;
use crate::bind_map::{MOUSE_BINDS, BindMap};
use crate::{Bind, BindableKey};

#[derive(Debug, Eq, PartialEq, Hash, Copy, Clone, EnumIter)]
pub enum MouseButton {
    LeftButton,
    MiddleButton,
    RightButton,
    X1Button,
    X2Button,

    #[strum(disabled)]
    OtherButton(u32),
}

pub struct MouseCursor;

pub struct MouseWheel;

impl BindableKey for MouseButton {
    fn bind(self, bind: Bind) {
        MOUSE_BINDS.insert_bind(self, bind);
    }

    fn unbind(self) -> Option<Bind> {
        MOUSE_BINDS.remove_bind(self)
    }
}
