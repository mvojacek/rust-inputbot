use std::sync::Arc;

pub enum Bind {
    NormalBind(BindHandler),
    #[cfg(target_os = "windows")]
    BlockBind(BlockBindHandler),
    #[cfg(target_os = "windows")]
    BlockableBind(BlockableBindHandler),
}

impl Bind {
    pub fn normal<F: Fn() + Send + Sync + 'static>(callback: F) -> Self {
        Self::NormalBind(Arc::new(callback))
    }
}

#[cfg(target_os = "windows")]
impl Bind {
    pub fn block<F: Fn() + Send + Sync + 'static>(callback: F) -> Self {
        Self::BlockBind(Arc::new(callback))
    }

    pub fn blockable<F: Fn() -> BlockInput + Send + Sync + 'static>(callback: F) -> Self {
        Self::BlockableBind(Arc::new(callback))
    }
}

type BindHandler = Arc<dyn Fn() + Send + Sync + 'static>;
#[cfg(target_os = "windows")]
type BlockBindHandler = Arc<dyn Fn() + Send + Sync + 'static>;
#[cfg(target_os = "windows")]
type BlockableBindHandler = Arc<dyn Fn() -> BlockInput + Send + Sync + 'static>;

pub enum BlockInput {
    Block,
    DontBlock,
}
