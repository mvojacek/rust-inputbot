use strum::IntoEnumIterator;
use crate::{Bind, BlockInput};
use std::hash::Hash;

pub trait BindableKey: Eq + Hash + Sized + Send + Sync + Copy + 'static {
    fn bind(self, bind: Bind);

    fn unbind(self) -> Option<Bind>;

    fn normal_bind<F: Fn() + Send + Sync + 'static>(self, callback: F) {
        self.bind(Bind::normal(callback));
    }

    #[cfg(target_os = "windows")]
    fn block_bind<F: Fn() + Send + Sync + 'static>(self, callback: F) {
        self.bind(Bind::block(callback));
    }

    #[cfg(target_os = "windows")]
    fn blockable_bind<F: Fn() -> BlockInput + Send + Sync + 'static>(self, callback: F) {
        self.bind(Bind::blockable(callback));
    }
}

pub trait BindableKeyAll: BindableKey + IntoEnumIterator {
    fn bind_all<F: Fn(Self) -> Bind + Send + Sync + 'static>(callback_gen: F) {
        for key in Self::iter() {
            key.bind(callback_gen(key));
        }
    }

    fn normal_bind_all<F: Fn(Self) + Send + Sync + Copy + 'static>(callback: F) {
        Self::bind_all(move |key| { Bind::normal(move || callback(key)) });
    }

    #[cfg(target_os = "windows")]
    fn block_bind_all<F: Fn(Self) + Send + Sync + Copy + 'static>(callback: F) {
        Self::bind_all(move |key| { Bind::block(move || callback(key)) })
    }

    #[cfg(target_os = "windows")]
    fn blockable_bind_all<F: Fn(Self) -> BlockInput + Send + Sync + Copy + 'static>(callback: F) {
        Self::bind_all(move |key| { Bind::blockable(move || callback(key)) })
    }
}

impl<K: BindableKey + IntoEnumIterator> BindableKeyAll for K {}
