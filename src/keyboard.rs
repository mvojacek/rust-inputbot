use std::thread::sleep;
use std::time::Duration;

use strum_macros::EnumIter;
use crate::bind_map::{KEYBD_BINDS, BindMap};
use crate::{Bind, BindableKey};
use std::convert::TryFrom;

#[derive(Debug, Eq, PartialEq, Hash, Copy, Clone, EnumIter)]
pub enum KeybdKey {
    BackspaceKey,
    TabKey,
    EnterKey,
    EscapeKey,
    SpaceKey,
    HomeKey,
    LeftKey,
    UpKey,
    RightKey,
    DownKey,
    InsertKey,
    DeleteKey,
    Numrow0Key,
    Numrow1Key,
    Numrow2Key,
    Numrow3Key,
    Numrow4Key,
    Numrow5Key,
    Numrow6Key,
    Numrow7Key,
    Numrow8Key,
    Numrow9Key,
    AKey,
    BKey,
    CKey,
    DKey,
    EKey,
    FKey,
    GKey,
    HKey,
    IKey,
    JKey,
    KKey,
    LKey,
    MKey,
    NKey,
    OKey,
    PKey,
    QKey,
    RKey,
    SKey,
    TKey,
    UKey,
    VKey,
    WKey,
    XKey,
    YKey,
    ZKey,
    Numpad0Key,
    Numpad1Key,
    Numpad2Key,
    Numpad3Key,
    Numpad4Key,
    Numpad5Key,
    Numpad6Key,
    Numpad7Key,
    Numpad8Key,
    Numpad9Key,
    F1Key,
    F2Key,
    F3Key,
    F4Key,
    F5Key,
    F6Key,
    F7Key,
    F8Key,
    F9Key,
    F10Key,
    F11Key,
    F12Key,
    F13Key,
    F14Key,
    F15Key,
    F16Key,
    F17Key,
    F18Key,
    F19Key,
    F20Key,
    F21Key,
    F22Key,
    F23Key,
    F24Key,
    NumLockKey,
    ScrollLockKey,
    CapsLockKey,
    LShiftKey,
    RShiftKey,
    LControlKey,
    RControlKey,

    #[strum(disabled)]
    OtherKey(u64),
}

impl TryFrom<KeybdKey> for char {
    type Error = ();

    fn try_from(key: KeybdKey) -> Result<Self, Self::Error> {
        from_keybd_key(key).ok_or(())
    }
}

impl TryFrom<char> for KeybdKey {
    type Error = ();

    fn try_from(c: char) -> Result<Self, Self::Error> {
        get_keybd_key(c).ok_or(())
    }
}

fn from_keybd_key(k: KeybdKey) -> Option<char> {
    use crate::KeybdKey::*;
    Some(match k {
        AKey => 'a',
        BKey => 'b',
        CKey => 'c',
        DKey => 'd',
        EKey => 'e',
        FKey => 'f',
        GKey => 'g',
        HKey => 'h',
        IKey => 'i',
        JKey => 'j',
        KKey => 'k',
        LKey => 'l',
        MKey => 'm',
        NKey => 'n',
        OKey => 'o',
        PKey => 'p',
        QKey => 'q',
        RKey => 'r',
        SKey => 's',
        TKey => 't',
        UKey => 'u',
        VKey => 'v',
        WKey => 'w',
        XKey => 'x',
        YKey => 'y',
        ZKey => 'z',
        Numpad0Key => '0',
        Numpad1Key => '1',
        Numpad2Key => '2',
        Numpad3Key => '3',
        Numpad4Key => '4',
        Numpad5Key => '5',
        Numpad6Key => '6',
        Numpad7Key => '7',
        Numpad8Key => '8',
        Numpad9Key => '9',
        Numrow0Key => '0',
        Numrow1Key => '1',
        Numrow2Key => '2',
        Numrow3Key => '3',
        Numrow4Key => '4',
        Numrow5Key => '5',
        Numrow6Key => '6',
        Numrow7Key => '7',
        Numrow8Key => '8',
        Numrow9Key => '9',
        _ => return None,
    })
}

fn get_keybd_key(c: char) -> Option<KeybdKey> {
    use crate::KeybdKey::*;
    Some(match c {
        ' ' => SpaceKey,
        'A' | 'a' => AKey,
        'B' | 'b' => BKey,
        'C' | 'c' => CKey,
        'D' | 'd' => DKey,
        'E' | 'e' => EKey,
        'F' | 'f' => FKey,
        'G' | 'g' => GKey,
        'H' | 'h' => HKey,
        'I' | 'i' => IKey,
        'J' | 'j' => JKey,
        'K' | 'k' => KKey,
        'L' | 'l' => LKey,
        'M' | 'm' => MKey,
        'N' | 'n' => NKey,
        'O' | 'o' => OKey,
        'P' | 'p' => PKey,
        'Q' | 'q' => QKey,
        'R' | 'r' => RKey,
        'S' | 's' => SKey,
        'T' | 't' => TKey,
        'U' | 'u' => UKey,
        'V' | 'v' => VKey,
        'W' | 'w' => WKey,
        'X' | 'x' => XKey,
        'Y' | 'y' => YKey,
        'Z' | 'z' => ZKey,
        _ => return None,
    })
}

pub struct KeySequence(pub &'static str);

impl KeySequence {
    pub fn send(&self) {
        for c in self.0.chars() {
            let mut uppercase = false;
            if let Some(keybd_key) = {
                if c.is_uppercase() {
                    uppercase = true;
                }
                get_keybd_key(c)
            } {
                if uppercase {
                    KeybdKey::LShiftKey.press();
                }
                keybd_key.press();
                sleep(Duration::from_millis(20));
                keybd_key.release();
                if uppercase {
                    KeybdKey::LShiftKey.release();
                }
            };
        }
    }
}

impl BindableKey for KeybdKey {
    fn bind(self, bind: Bind) {
        KEYBD_BINDS.insert_bind(self, bind);
    }

    fn unbind(self) -> Option<Bind> {
        KEYBD_BINDS.remove_bind(self)
    }
}
